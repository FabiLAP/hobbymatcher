class Person
  	attr_accessor :name, :hobbies

  	#constructor
	def initialize(args={})
		#looping all the keys of the hash
		args.each_key do |key|
			#check if there is a field like the key in the object 
			if respond_to?(key)
				#call the setter and pass the corressponding values
				send(key.to_s + '=', args[key])
			else	
				#the hash has a key where no field exists --> raise error
				raise UnknownAttributeError.new
			end
		end
	end

	#setter for the hobbies field
	def hobbies= (input)
		# check if input is null / nil
		if input
			#split the String into an array and remove whitespaces by looping and strip()
			@hobbies = input.split(',').map{|x| x.strip};
		end
	end 

	def commonHobbies (otherPerson) 
		#init the return array
		gotInCommon = [];
		
		#hobbies.each do |hobbie|
		#	otherPerson.hobbies.each do |other_hobbie|
		#		if hobbie == other_hobbie
		#			gotInCommon << hobbie;
		#		end
		#	end
		#end

		#looping each hobby of this person
		@hobbies.map { |thisPersonsHobby| 
			#if the current hobby is included in the hobbies of the argument-preson
			if otherPerson.hobbies.include?(thisPersonsHobby) 
				#save this hobby in the array
				gotInCommon << thisPersonsHobby;
			end
		}
		#return
		gotInCommon;
	end

	def friendslist (friendsArray)
		#init the return array
		gotInCommon = [];	
		#looping the firendsarray	
		while friendsArray.size > 1 do
			#compare all others with first object/person
			compareWith = friendsArray.first;
			###puts /n + 'compare with person: ' + compareWith.name;
			
			#delete first object/person of friendsArray to not consider when comparing
			friendsArray.shift;

			#looping the rest of firendsarray
			friendsArray.map { |friend|
				#init an array for saving the common hobbies for the current person
				samePeople = [];
				#looping the hobbies of the current person 
				compareWith.hobbies.map { |hobbiesOfCompareWith|

					###puts 'over all left freinds: ' + friend.name + friend.hobbies.to_s;
					###puts 'single hobby of compare with person: ' + hobbiesOfCompareWith; 

					#if they have common hobbies --> save the hobbies in the array
					if friend.hobbies.include?(hobbiesOfCompareWith) 
						samePeople << hobbiesOfCompareWith;
					end
				}

				#get the number on common hobbies
				numOfHobbiesInCommon = samePeople.size;
					#if they have common hobbies --> add all the information to the array which gets retruned
					if numOfHobbiesInCommon > 0
						gotInCommon << [numOfHobbiesInCommon, samePeople.join(', '), compareWith.name, friend.name]
					end

				#sort the array by the number of Common Hobbies
				gotInCommon.sort_by{numOfHobbiesInCommon};
			}
		end
		#return
		gotInCommon;
	#puts gotInCommon.to_s;
	end

end

class UnknownAttributeError < Exception

end


##########

=begin
kai_hash = {:name => "Kai", :hobbies => "bli, bla, blub"}
kai = Person.new(kai_hash);

person1 = {:name => "foo", :hobbies => "tata, bumbum, jojo"}
person1 = Person.new(person1);

person2 = {:name => "bar", :hobbies => "tata, yolo, moep"}
person2 = Person.new(person2);

puts person1.commonHobbies(person2);
=end

=begin
p1 = Person.new(name: "Hans",hobbies: "Diving")
p2 = Person.new(name: "Dagobert1",hobbies: "Money,Bathing,Knitting")
p3 = Person.new(name: "Dagobert2",hobbies: "Money,Bathing")
p4 = Person.new(name: "Donald",hobbies: "Bathing")
a = [p1,p2,p3,p4]
result = p1.friendslist(a)
=end

#puts kai;
#puts kai.name;
#puts kai.hobbies;

#fabian_hash = {:name => "Fabian", :hobbies => "kiten, geocachen, java", :wrongAttribut => 'fehler'}
#fabian = Person.new(fabian_hash);